package com.example.ing.ingtest.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Setter
@Getter
@ToString
@Document(collection = "exchangeRate")
public class ExchangeRate {

    private String id;
    private String currencyFrom;
    private String currencyTo;
    private Float rate;

    public ExchangeRate() {
        id = new ObjectId().toString();
    }
}
