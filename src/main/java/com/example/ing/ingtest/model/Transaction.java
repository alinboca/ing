package com.example.ing.ingtest.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@Document(collection = "transaction")
public class Transaction {
    @Id
    private String id;
    private String accountId;
    private ExchangeRate exchangeRate;
    private OriginalAmount originalAmount;
    private Tor creditor;
    private Tor debtor;
    private String status;
    private String currency;
    private BigDecimal amount;
    private LocalDateTime update;
    private String description;
}
