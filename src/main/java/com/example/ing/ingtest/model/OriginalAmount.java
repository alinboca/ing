package com.example.ing.ingtest.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Getter
@Setter
@Accessors(chain = true)
@ToString
@Document(collection = "originalAmount")
public class OriginalAmount {

    private String id;
    private BigDecimal amount;
    private String currency;

    public OriginalAmount() {
        id = new ObjectId().toString();
    }
}
