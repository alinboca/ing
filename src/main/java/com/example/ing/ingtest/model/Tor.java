package com.example.ing.ingtest.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Accessors(chain = true)
@ToString
@Document(collection = "tor")
public class Tor {

    private String id;
    private String maskedPan;
    private String name;

    public Tor(){
        id = new ObjectId().toString();
    }
}
