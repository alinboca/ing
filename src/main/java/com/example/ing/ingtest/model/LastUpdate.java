package com.example.ing.ingtest.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Setter
@Getter
@ToString
@Document(collection = "lastUpdate")
public class LastUpdate {
    @Id
    private String username;
    private String token;
    private LocalDateTime accountlastUpdate;
    private LocalDateTime transactionLastUpdate;

}
