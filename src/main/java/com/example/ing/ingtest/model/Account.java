package com.example.ing.ingtest.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@ToString
@Document(collection = "account")
public class Account {
    @Id
    private String id;
    private String username;
    private LocalDateTime update;
    private String name;
    private String product;
    private String status;
    private String type;
    private BigDecimal balance;
}
