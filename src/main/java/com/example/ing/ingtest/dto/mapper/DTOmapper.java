package com.example.ing.ingtest.dto.mapper;

import com.example.ing.ingtest.dto.model.*;
import com.example.ing.ingtest.model.*;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class DTOmapper {

    public AccountDTO mapAccountDTO(Account account){
        return new AccountDTO().setId(account.getId())
                .setUpdate(account.getUpdate())
                .setName(account.getName())
                .setProduct(account.getProduct())
                .setStatus(account.getStatus())
                .setType(account.getType())
                .setBalance(account.getBalance());
    }

    public TransactionDTO mapTransactionDTO(Transaction transaction){
        return new TransactionDTO()
                .setId(transaction.getId())
                .setAccountId(transaction.getAccountId())
                .setExchangeRate(new ModelMapper().map(transaction.getExchangeRate(),ExchangeRateDTO.class))
                .setOriginalAmount(new ModelMapper().map(transaction.getOriginalAmount(),OriginalAmountDTO.class))
                .setCreditor(new ModelMapper().map(transaction.getCreditor(),TorDTO.class))
                .setDebtor(new ModelMapper().map(transaction.getDebtor(),TorDTO.class))
                .setStatus(transaction.getStatus())
                .setCurrency(transaction.getCurrency())
                .setAmount(transaction.getAmount())
                .setUpdate(transaction.getUpdate())
                .setDescription(transaction.getDescription());

    }

    public Transaction mapTransaction(TransactionDTO transactionDTO){
       Transaction transaction = new Transaction();
        transaction.setId(transactionDTO.getId());
        transaction.setAccountId(transactionDTO.getAccountId());
        transaction.setExchangeRate(new ModelMapper().map(transactionDTO.getExchangeRate(),ExchangeRate.class));
        transaction.setOriginalAmount(new ModelMapper().map(transactionDTO.getOriginalAmount(),OriginalAmount.class));
        transaction.setCreditor(new ModelMapper().map(transactionDTO.getCreditor(),Tor.class));
        transaction.setDebtor(new ModelMapper().map(transactionDTO.getDebtor(),Tor.class));
        transaction.setStatus(transactionDTO.getStatus());
        transaction.setCurrency(transactionDTO.getCurrency());
        transaction.setAmount(transactionDTO.getAmount());
        transaction.setUpdate(transactionDTO.getUpdate());
        transaction.setDescription(transactionDTO.getDescription());
        return transaction;
    }

    public Account mapAccount(AccountDTO accountDTO){
        Account account = new Account();
        account.setId(accountDTO.getId());
        account.setUpdate(accountDTO.getUpdate());
        account.setName(accountDTO.getName());
        account.setProduct(accountDTO.getProduct());
        account.setStatus(accountDTO.getStatus());
        account.setType(accountDTO.getType());
        account.setBalance(accountDTO.getBalance());
        return account;
    }
}
