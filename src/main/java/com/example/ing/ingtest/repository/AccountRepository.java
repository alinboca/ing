package com.example.ing.ingtest.repository;

import com.example.ing.ingtest.model.Account;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends MongoRepository<Account,String> {
    List<Account> findAllByUsername(String username);
}
