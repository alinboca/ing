package com.example.ing.ingtest.repository;

import com.example.ing.ingtest.model.LastUpdate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LastUpdateRepository extends MongoRepository<LastUpdate,String> {

    LastUpdate findByUsername(String username);
}
