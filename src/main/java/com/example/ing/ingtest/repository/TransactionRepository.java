package com.example.ing.ingtest.repository;

import com.example.ing.ingtest.model.Transaction;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends MongoRepository<Transaction,String> {
    List<Transaction> findAllByAccountId(String accountId);
}
