package com.example.ing.ingtest.service;

import com.example.ing.ingtest.dto.model.AccountDTO;
import com.example.ing.ingtest.dto.model.TokenDTO;
import com.example.ing.ingtest.dto.model.TransactionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class WiremockServiceImp implements WiremockService {

    @Autowired
    private RestService restService;

    @Value("${wiremock.login.url}")
    private String loginUrl;

    @Value("${wiremock.accounts.url}")
    private String accountsUrl;

    @Value("${wiremock.transactions.url}")
    private String transactionsUrl;

    @Override
    public Optional<TokenDTO> login(String username) {
        Map<String,Object> postParameters = new HashMap<>();
        postParameters.put("username", username);
        return restService.post(loginUrl,postParameters,TokenDTO.class);
    }

    @Override
    public Optional<List<AccountDTO>> getAccounts(TokenDTO token) {
        Map<String,String> getParameters = new HashMap<>();
        getParameters.put("username", token.getToken());
        return restService.get(accountsUrl, getParameters, new ParameterizedTypeReference<List<AccountDTO>>() {});
    }

    @Override
    public Optional<List<TransactionDTO>> getTransactions(TokenDTO token) {
        Map<String,String> getParameters = new HashMap<>();
        getParameters.put("username", token.getToken());
        return restService.get(accountsUrl, getParameters, new ParameterizedTypeReference<List<TransactionDTO>>() {});
    }
}
