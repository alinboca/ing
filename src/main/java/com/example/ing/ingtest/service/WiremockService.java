package com.example.ing.ingtest.service;

import com.example.ing.ingtest.dto.model.AccountDTO;
import com.example.ing.ingtest.dto.model.TokenDTO;
import com.example.ing.ingtest.dto.model.TransactionDTO;

import java.util.List;
import java.util.Optional;

public interface WiremockService {
    Optional<TokenDTO> login(String username);
    Optional<List<AccountDTO>> getAccounts(TokenDTO token);
    Optional<List<TransactionDTO>> getTransactions(TokenDTO token);
}
