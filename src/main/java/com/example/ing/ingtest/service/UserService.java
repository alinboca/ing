package com.example.ing.ingtest.service;

import com.example.ing.ingtest.dto.model.AccountDTO;
import com.example.ing.ingtest.dto.model.TokenDTO;
import com.example.ing.ingtest.dto.model.TransactionDTO;
import com.example.ing.ingtest.model.LastUpdate;
import com.example.ing.ingtest.repository.LastUpdateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Scope(value = "prototype", proxyMode=ScopedProxyMode.TARGET_CLASS)
public class UserService {

    @Autowired
    private WiremockService wiremockService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private LastUpdateRepository lastUpdateRepository;

    private TokenDTO token;
    private LastUpdate lastUpdate;

    public boolean login(String username) {
        //TODO: implement auth
        this.token = wiremockService.login(username)
                .orElseThrow(() -> new UnsupportedOperationException("User not found"));
        this.lastUpdate = lastUpdateRepository.findByUsername(username);
        return true;
    }

    public List<AccountDTO> getAccounts(String username) {
        List<AccountDTO> savedAccounts = repositoryService.getAccounts(username);
        if (savedAccounts.isEmpty() && lastUpdate.getAccountlastUpdate().plusDays(1).isBefore(LocalDateTime.now())) {
            List<AccountDTO> accounts = wiremockService.getAccounts(token)
                    .orElseThrow(() -> new RuntimeException("No accounts present"));
            if(accounts.isEmpty()){
                repositoryService.saveAccounts(accounts);
                lastUpdate.setAccountlastUpdate(LocalDateTime.now());
                lastUpdateRepository.save(lastUpdate);
                return accounts;
            }
        }
        return savedAccounts;
    }

    public List<TransactionDTO> getTransactions(String accountId) {
        List<TransactionDTO> savedTransaction = repositoryService.getTransactions(accountId);
        if (savedTransaction.isEmpty() && lastUpdate.getTransactionLastUpdate().plusDays(1).isBefore(LocalDateTime.now())) {
            Optional<List<TransactionDTO>> transactions = wiremockService.getTransactions(token);
            if(transactions.isPresent()){
                List<TransactionDTO> transactionDTOS = transactions.get();
                repositoryService.saveTransactions(transactionDTOS);
                lastUpdate.setTransactionLastUpdate(LocalDateTime.now());
                lastUpdateRepository.save(lastUpdate);
                return transactionDTOS;
            }
        }
        return savedTransaction;
    }
}
