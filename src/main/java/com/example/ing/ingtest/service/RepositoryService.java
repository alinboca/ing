package com.example.ing.ingtest.service;

import com.example.ing.ingtest.dto.mapper.DTOmapper;
import com.example.ing.ingtest.dto.model.AccountDTO;
import com.example.ing.ingtest.dto.model.TransactionDTO;
import com.example.ing.ingtest.repository.AccountRepository;
import com.example.ing.ingtest.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RepositoryService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private DTOmapper dtoMapper;

    @Transactional
    public List<TransactionDTO> getTransactions(String accountId) {
        return transactionRepository.findAllByAccountId(accountId).stream()
                .map(dtoMapper::mapTransactionDTO)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<AccountDTO> getAccounts(String username) {
        return accountRepository.findAllByUsername(username).stream()
                .map(dtoMapper::mapAccountDTO)
                .collect(Collectors.toList());
    }

    @Transactional
    public void saveAccounts(List<AccountDTO> accounts) {
        accountRepository.saveAll(accounts.stream()
                .map(dtoMapper::mapAccount)
                .collect(Collectors.toList()));
    }

    @Transactional
    public void saveTransactions(List<TransactionDTO> accounts) {
        transactionRepository.saveAll(accounts.stream()
                .map(dtoMapper::mapTransaction)
                .collect(Collectors.toList()));
    }
}
