package com.example.ing.ingtest.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.*;

@Service
public class RestService {

    private final RestTemplate restTemplate;

    @Value("${rest.timeout}")
    private long timeout;

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder
                .setConnectTimeout(Duration.ofSeconds(timeout))
                .setReadTimeout(Duration.ofSeconds(timeout))
                .build();
    }

    public <T> Optional<T> post(final String URL, Map<String, Object> postParameters, Class<T> clazz) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(postParameters, headers);
        ResponseEntity<T> response = restTemplate.postForEntity(URL, entity, clazz);

        if (response.getStatusCode() == HttpStatus.OK)
            return Optional.ofNullable(response.getBody());

        return Optional.empty();
    }

    public <T> Optional<List<T>> get(final String URL, Map<String, String> getParameters, ParameterizedTypeReference<List<T>> typeReference) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        getParameters.forEach(headers::set);

        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<List<T>> response = this.restTemplate.exchange(URL, HttpMethod.GET, request, typeReference);

        if (response.getStatusCode() == HttpStatus.OK)
            return Optional.ofNullable(response.getBody());

        return Optional.empty();
    }
}
